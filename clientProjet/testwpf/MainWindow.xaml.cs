﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MySql.Data.MySqlClient;

namespace testwpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Label> plat = new List<Label>();
        List<Label> prix = new List<Label>();
        List<ComboBox> qu = new List<ComboBox>();
        List<CheckBox> check = new List<CheckBox>();

        List<String> platSe = new List<String>();
        List<String> prixSe = new List<String>();
        List<int> quSe = new List<int>();
        public MainWindow()
        {
            InitializeComponent();

            /*Label b = new Label();
            Label c = new Label();
            b.Content = "b";
            c.Content = "c";
            b.HorizontalAlignment = HorizontalAlignment.Left;
            b.VerticalAlignment = VerticalAlignment.Top;
            c.HorizontalAlignment = HorizontalAlignment.Left;
            c.VerticalAlignment = VerticalAlignment.Top;
            c.Margin = new Thickness(50, 50, 0, 0);
           
            b.Margin = new Thickness(1000, 200, 0, 0);
            

            
            Canvas can = new Canvas();
            can.Children.Add(b);
            can.Children.Add(c);
            this.Content = can;*/

            comm.Click += new System.Windows.RoutedEventHandler(this.comm_clicked);

            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=");
            connection.Open();
           
            int i = 0;
            MySqlCommand oCommand1 = new MySqlCommand("select * from restaurant.entreeschaudes", connection);
            MySqlDataReader reader1 = oCommand1.ExecuteReader();
            
             int diff = 180;
             Object[] ItemObject = new Object[10];
            remplir(ct);
            ct.Items.RemoveAt(0);
            ct.SelectedIndex = 0;

            while (reader1.Read())
             {
                 plat.Add(new Label());
                 prix.Add(new Label());
                 qu.Add(new ComboBox());
                 check.Add(new CheckBox());

                remplir(qu[i]);
                qu[i].SelectedIndex = 0;
                qu[i].IsEnabled = false;
                check[i].Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
                check[i].Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Unchecked);


                plat[i].Content =  reader1[0].ToString();
                 prix[i].Content = (float) reader1[1]+ " DH" ;

                plat[i].FontFamily = new FontFamily("MV Boli");
                prix[i].FontFamily = new FontFamily("MV Boli");
                plat[i].FontSize = 16 ;
                prix[i].FontSize = 16 ;

                plat[i].Foreground = new SolidColorBrush(Colors.White);
                prix[i].Foreground = new SolidColorBrush(Colors.White);



                plat[i].Width = 150;
                prix[i].Width = 60;
                qu[i].Width = 40;
                check[i].Width = 30;

                plat[i].Height = 30 ;
                prix[i].Height =30 ;
                qu[i].Height = 20;
                check[i].Height = 30;
                

                plat[i].Margin = new Thickness(0, diff, 0, 0);
                prix[i].Margin = new Thickness(150, diff, 0, 0);
                check[i].Margin = new Thickness(215, diff + 10, 0, 0);
                qu[i].Margin = new Thickness(240, diff+5, 0, 0);

                p.Children.Add(plat[i]);
                p.Children.Add(prix[i]);
                p.Children.Add(qu[i]);
                p.Children.Add(check[i]);

                i++;
                diff += 30;
             }
            diff += 10;
            f.Margin = new Thickness(85, diff , 0, 0);
            diff += 40;
            connection.Close();
            connection.Open();
            MySqlCommand oCommand2 = new MySqlCommand("select * from restaurant.entreesfroides", connection);
            MySqlDataReader reader2 = oCommand2.ExecuteReader();
            while (reader2.Read())
            {
                plat.Add(new Label());
                prix.Add(new Label());
                qu.Add(new ComboBox());
                check.Add(new CheckBox());

                remplir(qu[i]);
                qu[i].SelectedIndex = 0;
                qu[i].IsEnabled = false;
                check[i].Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
                check[i].Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Unchecked);

                plat[i].Content = reader2[0].ToString();
                prix[i].Content = (float)reader2[1] + " DH";

                plat[i].FontFamily = new FontFamily("MV Boli");
                prix[i].FontFamily = new FontFamily("MV Boli");
                plat[i].FontSize = 16;
                prix[i].FontSize = 16;

                plat[i].Foreground = new SolidColorBrush(Colors.White);
                prix[i].Foreground = new SolidColorBrush(Colors.White);



                plat[i].Width = 160;
                prix[i].Width = 60;
                qu[i].Width = 40;
                check[i].Width = 30;

                plat[i].Height = 30;
                prix[i].Height = 30;
                qu[i].Height = 20;
                check[i].Height = 30;


                plat[i].Margin = new Thickness(0, diff, 0, 0);
                prix[i].Margin = new Thickness(150, diff, 0, 0);
                check[i].Margin = new Thickness(215, diff + 10, 0, 0);
                qu[i].Margin = new Thickness(240, diff + 5, 0, 0);

                p.Children.Add(plat[i]);
                p.Children.Add(prix[i]);
                p.Children.Add(qu[i]);
                p.Children.Add(check[i]);

                i++;
                diff += 30;
            }

            diff = 170;
            connection.Close();
            connection.Open();
            MySqlCommand oCommand3 = new MySqlCommand("select * from restaurant.platdivers", connection);
            MySqlDataReader reader3 = oCommand3.ExecuteReader();
            while (reader3.Read())
            {
                plat.Add(new Label());
                prix.Add(new Label());
                qu.Add(new ComboBox());
                check.Add(new CheckBox());

                remplir(qu[i]);
                qu[i].SelectedIndex = 0;
                qu[i].IsEnabled = false;
                check[i].Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
                check[i].Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Unchecked);

                plat[i].Content = reader3[0].ToString();
                prix[i].Content = (float)reader3[1] + " DH";

                plat[i].FontFamily = new FontFamily("MV Boli");
                prix[i].FontFamily = new FontFamily("MV Boli");
                plat[i].FontSize = 16;
                prix[i].FontSize = 16;

                plat[i].Foreground = new SolidColorBrush(Colors.White);
                prix[i].Foreground = new SolidColorBrush(Colors.White);



                plat[i].Width = 160;
                prix[i].Width = 60;
                qu[i].Width = 40;
                check[i].Width = 30;

                plat[i].Height = 30;
                prix[i].Height = 30;
                qu[i].Height = 20;
                check[i].Height = 30;


                plat[i].Margin = new Thickness(300, diff, 0, 0);
                prix[i].Margin = new Thickness(450, diff, 0, 0);
                check[i].Margin = new Thickness(515, diff + 10, 0, 0);
                qu[i].Margin = new Thickness(540, diff + 5, 0, 0);

                p.Children.Add(plat[i]);
                p.Children.Add(prix[i]);
                p.Children.Add(qu[i]);
                p.Children.Add(check[i]);

                i++;
                diff += 30;
            }
            diff = 170;
            connection.Close();
            connection.Open();
            MySqlCommand oCommand4 = new MySqlCommand("select * from restaurant.boissons", connection);
            MySqlDataReader reader4 = oCommand4.ExecuteReader();
            while (reader4.Read())
            {
                plat.Add(new Label());
                prix.Add(new Label());
                qu.Add(new ComboBox());
                check.Add(new CheckBox());

                remplir(qu[i]);
                qu[i].SelectedIndex = 0;
                qu[i].IsEnabled = false;
                check[i].Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
                check[i].Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Unchecked);

                plat[i].Content = reader4[0].ToString();
                prix[i].Content = (float)reader4[1] + " DH";

                plat[i].FontFamily = new FontFamily("MV Boli");
                prix[i].FontFamily = new FontFamily("MV Boli");
                plat[i].FontSize = 16;
                prix[i].FontSize = 16;

                plat[i].Foreground = new SolidColorBrush(Colors.White);
                prix[i].Foreground = new SolidColorBrush(Colors.White);



                plat[i].Width = 160;
                prix[i].Width = 60;
                qu[i].Width = 40;
                check[i].Width = 30;

                plat[i].Height = 30;
                prix[i].Height = 30;
                qu[i].Height = 20;
                check[i].Height = 30;


                plat[i].Margin = new Thickness(600, diff, 0, 0);
                prix[i].Margin = new Thickness(750, diff, 0, 0);
                check[i].Margin = new Thickness(815, diff + 10, 0, 0);
                qu[i].Margin = new Thickness(840, diff + 5, 0, 0);

                p.Children.Add(plat[i]);
                p.Children.Add(prix[i]);
                p.Children.Add(qu[i]);
                p.Children.Add(check[i]);

                i++;
                diff += 30;
            }
            diff = 170;
            connection.Close();
            connection.Open();
            MySqlCommand oCommand5 = new MySqlCommand("select * from restaurant.dessert", connection);
            MySqlDataReader reader5 = oCommand5.ExecuteReader();
            while (reader5.Read())
            {
                plat.Add(new Label());
                prix.Add(new Label());
                qu.Add(new ComboBox());
                check.Add(new CheckBox());

                remplir(qu[i]);
                qu[i].SelectedIndex = 0;
                qu[i].IsEnabled = false;
                check[i].Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
                check[i].Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Unchecked);

                plat[i].Content = reader5[0].ToString();
                prix[i].Content = (float)reader5[1] + " DH";

                plat[i].FontFamily = new FontFamily("MV Boli");
                prix[i].FontFamily = new FontFamily("MV Boli");
                plat[i].FontSize = 16;
                prix[i].FontSize = 16;

                plat[i].Foreground = new SolidColorBrush(Colors.White);
                prix[i].Foreground = new SolidColorBrush(Colors.White);



                plat[i].Width = 160;
                prix[i].Width = 60;
                qu[i].Width = 40;
                check[i].Width = 30;

                plat[i].Height = 30;
                prix[i].Height = 30;
                qu[i].Height = 20;
                check[i].Height = 30;


                plat[i].Margin = new Thickness(900, diff, 0, 0);
                prix[i].Margin = new Thickness(1050, diff, 0, 0);
                check[i].Margin = new Thickness(1115, diff + 10, 0, 0);
                qu[i].Margin = new Thickness(1140, diff + 5, 0, 0);

                p.Children.Add(plat[i]);
                p.Children.Add(prix[i]);
                p.Children.Add(qu[i]);
                p.Children.Add(check[i]);

                i++;
                diff += 30;
            }

           
    }
        public void remplir(ComboBox ch)
        {

            for (int j = 0; j <= 9; j++)
            {
                ch.Items.Insert(j, j);
            }
           


    }
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
           for( int i = 0; i< check.Count; i++)
            {
                if( sender.Equals(check[i]))
                {
                    qu[i].IsEnabled = true;
                    qu[i].SelectedIndex = 1;
                    comm.IsEnabled = true;
                    ct.IsEnabled = true;
                }
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            bool b = false;
            for (int i = 0; i < check.Count; i++)
            {
                if (sender.Equals(check[i]))
                {
                    qu[i].IsEnabled = false;
                    qu[i].SelectedIndex = 0;
                }
                if( check[i].IsChecked == true)
                {
                    b = true;
                }
            }
            if( b== false)
            {
                comm.IsEnabled = false;
                ct.IsEnabled = false;
            }
        }
        private void comm_clicked(object sender, RoutedEventArgs e)
        {
            for( int i =0;i< plat.Count; i++)
            {
                if( check[i].IsChecked == true )
                {
                    platSe.Add((String)plat[i].Content);
                    prixSe.Add((String) prix[i].Content);
                    quSe.Add(qu[i].SelectedIndex);

                }
            }
            commande c = new commande(platSe, prixSe, quSe, (int) ct.SelectedItem);
            c.Show();

            this.Close();
        }

        
    }
}
