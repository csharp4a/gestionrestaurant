﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace testwpf
{
    /// <summary>
    /// Interaction logic for commande.xaml
    /// </summary>
    public partial class commande : Window
    {
        List<String> plat;
        List<String> prix;
        List<int> qu;
        int item ;

        List<Label> platL = new List<Label>();
        List<Label> prixL = new List<Label>();
        List<Label> quL = new List<Label>();

        Stream ns;
        BinaryFormatter bf;
        public commande(List<String> plat,List<String> prix,List<int> qu , int ii)
        {
            InitializeComponent();
            this.plat = plat;
            this.prix = prix;
            this.qu = qu;
            item = ii;
            int diff = 150;
            float pt=0;
            TcpClient client = new TcpClient("localhost", 2000);

            ns = client.GetStream();

            bf = new BinaryFormatter();

            bf.Serialize(ns,"commander");
            bf.Serialize(ns, item);
            bf.Serialize(ns, plat);
            bf.Serialize(ns, qu);

            for (int i=0;i<plat.Count;i++)
            {
                platL.Add(new Label());
                prixL.Add(new Label());
                quL.Add(new Label());

                platL[i].Content = plat[i];
                prixL[i].Content = prix[i];
                quL[i].Content = qu[i];
                 
                platL[i].FontFamily = new FontFamily("MV Boli");
                prixL[i].FontFamily = new FontFamily("MV Boli");
                quL[i].FontFamily = new FontFamily("MV Boli");

                platL[i].FontSize = 20;
                prixL[i].FontSize = 20;
                quL[i].FontSize = 20;

                platL[i].Foreground = new SolidColorBrush(Colors.White);
                prixL[i].Foreground = new SolidColorBrush(Colors.White);
                quL[i].Foreground = new SolidColorBrush(Colors.White);


                platL[i].Width = 200;
                prixL[i].Width = 100;
                quL[i].Width = 600;
               

                platL[i].Height = 40;
                prixL[i].Height = 40;
                quL[i].Height = 40;
               


                platL[i].Margin = new Thickness(50, diff, 0, 0);
                quL[i].Margin = new Thickness(310, diff , 0, 0);
                prixL[i].Margin = new Thickness(420, diff, 0, 0);

                p.Children.Add(platL[i]);
                p.Children.Add(prixL[i]);
                p.Children.Add(quL[i]);

                pt += recup_int(prix[i]) * qu[i];





                diff += 40;
            }
            
            
            t2.Content = pt+ " DH";

            t1.Margin = new Thickness(41, diff+20, 0, 0);
            t2.Margin = new Thickness(41, diff+20, 0, 0);
            st.Margin = new Thickness(41, diff+70, 0, 0);
            st2.Margin = new Thickness(41, diff+ 70, 0, 0);
            payer.Margin = new Thickness(41, diff+ 140, 0, 0);
            p.Children.Remove(payer);
            payer.Click += new System.Windows.RoutedEventHandler(this.payer_clicked);


            Thread t = new Thread(() =>
            {
                while (true)
                {
                    String tmp = (String) bf.Deserialize(ns);
                    var dispatcher = Application.Current.Dispatcher;
                    if ( tmp.Equals("prepa"))
                    {
                        dispatcher.Invoke((Action)(() =>
                        {
                            st2.Content = "En cours de préparation";
                        }));
                    }
                    else if (tmp.Equals("serv"))
                    {
                        dispatcher.Invoke((Action)(() =>
                        {
                            st2.Content = "Commande Servie";
                            p.Children.Add(payer);

                        }));
                    }
                }
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();




        }
        private void payer_clicked(object sender, RoutedEventArgs e)
        {
            MainWindow w = new MainWindow();
            bf.Serialize(ns, "payer");
            bf.Serialize(ns, plat);
            bf.Serialize(ns, qu);
            bf.Serialize(ns, prix);
            w.Show();
            this.Close();
        }
        int recup_int(String c)
        {
            String tmp = "";
            for(int i=0; i< c.Length; i++)
            {
                if (c[i] == ' ')
                {
                    break;
                }
                tmp += c[i];
            }
            int tmp2 = int.Parse(tmp);
            return tmp2;
        }
    }
    
}
