-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 02 Février 2016 à 18:56
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `restaurant`
--

-- --------------------------------------------------------

--
-- Structure de la table `boissons`
--

CREATE TABLE IF NOT EXISTS `boissons` (
  `boisson` varchar(45) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`boisson`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `boissons`
--

INSERT INTO `boissons` (`boisson`, `prix`) VALUES
('Café ', 7),
('Jus d''Orange', 10),
('Limonades', 6),
('Thé ', 15);

-- --------------------------------------------------------

--
-- Structure de la table `dessert`
--

CREATE TABLE IF NOT EXISTS `dessert` (
  `plat` varchar(45) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`plat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `dessert`
--

INSERT INTO `dessert` (`plat`, `prix`) VALUES
('Flan', 10),
('Fromage', 15),
('Glâce', 30),
('Pâtisserie tunisienne', 15),
('Salade  de fruit', 25);

-- --------------------------------------------------------

--
-- Structure de la table `entreeschaudes`
--

CREATE TABLE IF NOT EXISTS `entreeschaudes` (
  `plat` varchar(45) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`plat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `entreeschaudes`
--

INSERT INTO `entreeschaudes` (`plat`, `prix`) VALUES
('Brick fromage', 13),
('Omelette fromage', 15);

-- --------------------------------------------------------

--
-- Structure de la table `entreesfroides`
--

CREATE TABLE IF NOT EXISTS `entreesfroides` (
  `plat` varchar(45) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`plat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `entreesfroides`
--

INSERT INTO `entreesfroides` (`plat`, `prix`) VALUES
('Salade Espagnol', 17),
('Salade Italienne', 17);

-- --------------------------------------------------------

--
-- Structure de la table `platdivers`
--

CREATE TABLE IF NOT EXISTS `platdivers` (
  `plat` varchar(45) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`plat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `platdivers`
--

INSERT INTO `platdivers` (`plat`, `prix`) VALUES
('Couscous 7 Vég', 65),
('Poivron farci', 30),
('Riz mouton', 45),
('Tagine Olive', 55);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
