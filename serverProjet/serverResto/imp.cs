﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imp
{
    class Impr
    {
        List<string> plats;
        List<string> prix;
        List<int> qte;

        public Impr(List<string> plats, List<string> prix, List<int> qte)
        {
            this.plats = plats;
            this.prix = prix;
            this.qte = qte;
        }

        public float calculTotal()
        {
            float somme = 0;


            using (var e1 = prix.GetEnumerator())
            using (var e2 = qte.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext())
                {
                    var item1 = e1.Current;
                    int item2 = e2.Current;

                    somme += recup_int(item1) * item2;
                }

            }

            return somme;
        }
        float recup_int(String c)
        {
            String tmp = "";
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == ' ')
                {
                    break;
                }
                tmp += c[i];
            }
            float tmp2 = float.Parse(tmp);
            return tmp2;
        }

        public void saveFacture()
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@".\WriteLines2.txt"))
            {
                file.WriteLine("\t    RESTAURANT");
                for (int i = 0; i < 2; i++)
                {
                    file.WriteLine("");
                }
                file.WriteLine("PLAT" + "             " + "QTE" + "             " + "PRIX");
                file.WriteLine("---------------------------------------");
                using (var e1 = plats.GetEnumerator())
                using (var e2 = qte.GetEnumerator())
                using (var e3 = prix.GetEnumerator())
                {
                    while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                    {
                        var item1 = e1.Current;
                        var item2 = e2.Current;
                        var item3 = e3.Current;

                        file.WriteLine(item1 + "             " + item2 + "             " + item3);
                    }

                    file.WriteLine("---------------------------------------");
                    for (int i = 0; i < 5; i++)
                    {
                        file.WriteLine("");
                    }
                    file.WriteLine("\t\t\tTOTAL :" + calculTotal());
                }


            }


        }
    }

   
}
