﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection.Emit;
using Imp;

namespace ConsoleServer
{
    class Program
    {
        static TcpListener clients;
        static TcpListener cuisinier;
        static int count = 0;
        static readonly object o = new object();
        static Socket socketCuis;
        static Socket socketC;
        static List<Socket> l;
        static Stream ns;
        static Stream nsc;
        static BinaryFormatter bf;
        static void Main(string[] args)
        {

            IPHostEntry host = Dns.GetHostEntry("localhost");
            IPAddress ip = new IPAddress(host.AddressList.First().GetAddressBytes());

            bf = new BinaryFormatter();
            l = new List<Socket>();

            cuisinier = new TcpListener(ip, 2001);
            cuisinier.Start();
            socketCuis = cuisinier.AcceptSocket();
            nsc = new NetworkStream(socketCuis);
            Console.WriteLine("Connected " + socketCuis.RemoteEndPoint);

            clients = new TcpListener(ip, 2000);
            clients.Start();
            socketC = clients.AcceptSocket();
            ns = new NetworkStream(socketC);
            Console.WriteLine("Connected " + socketC.RemoteEndPoint);

            new Thread(() =>
            {
               while (true)
                {
                    Socket s = cuisinier.AcceptSocket();
                    if (s != null)
                    {
                        nsc = new NetworkStream(s);
                        Console.WriteLine("Connected " + socketCuis.RemoteEndPoint);
                    }

                }
            }).Start();

            new Thread(() =>
            {
                while (true)
                {
                    String tmp = (String) bf.Deserialize(nsc);
                    if( tmp.Equals("prepa"))
                    {
                        bf.Serialize(ns, "prepa");
                    }
                    if (tmp.Equals("serv"))
                    {
                        bf.Serialize(ns, "serv");
                    }
                }
            }).Start();


            new Thread(() =>
            {
                /*int i = 1;
                while (true)
                {
                    Socket tmp = clients.AcceptSocket();
                    l.Add(tmp);
                    ns = new NetworkStream(tmp);
                    Console.WriteLine("Connected " + l[i].RemoteEndPoint);
                    i++;
                }*/
                while(true)
                {
                    Socket s = clients.AcceptSocket();
                    if (s != null)
                    {
                        ns = new NetworkStream(s);
                        Console.WriteLine("Connected " + s.RemoteEndPoint);
                    }
                }
            }).Start();

            new Thread(() =>
            {
                while (true)
                {
                    String tmp = (String)bf.Deserialize(ns);
                    Console.WriteLine(tmp);
                    if (tmp.Equals("commander"))
                    {
                        int numT = (int)bf.Deserialize(ns);
                        List<String> plat = (List<String>)bf.Deserialize(ns);
                        List<int> qu = (List<int>)bf.Deserialize(ns);
                        bf.Serialize(nsc, "commander");
                        bf.Serialize(nsc, numT);
                        bf.Serialize(nsc, plat);
                        bf.Serialize(nsc, qu);
                    }
                    else if( tmp.Equals("payer"))
                    {
                        List<String> plat = (List<String>)bf.Deserialize(ns);
                        List<int> qu = (List<int>)bf.Deserialize(ns);
                        List<String> prix = (List<String>)bf.Deserialize(ns);
                        Impr i = new Impr(plat, prix, qu);
                        i.saveFacture();
                    }
                }
            }).Start();



        }
        public static void ServerService()
        {
            Socket socket = clients.AcceptSocket();

            Console.WriteLine("Connected " + socket.RemoteEndPoint);

            Stream ns = new NetworkStream(socket);
            Stream nsC = new NetworkStream(socketCuis);
            

            var bf = new BinaryFormatter();

            bool client = false;

            new Thread(() =>
            {
                while (!client)
                {
                    // client -> cuisinier
                    String tmp = (String) bf.Deserialize(ns);
                    if( tmp.Equals("commander"))
                    {
                        int numT = ( int )bf.Deserialize(ns);
                        List <String> plat = (List<String>) bf.Deserialize(ns);
                        List <int> qu = (List<int>) bf.Deserialize(ns);
                        bf.Serialize(nsC, numT);
                        bf.Serialize(nsC, plat);
                        bf.Serialize(nsC, qu);

                    }
                   // Console.WriteLine(tmp);
                    //bf.Serialize( ns , tmp );
                    // cuisinier -> client
                    /*String tmp1 = (String)bf.Deserialize(nsC);
                    Console.WriteLine(tmp1);
                    bf.Serialize(ns, tmp1);*/
                }
            }).Start();
        }
    }
}