﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfCuisinier
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static TcpClient cuisinier;
        static Stream ns;
        static BinaryFormatter bf ;

        static List<Label> platL = new List<Label>();
        static List<Label> quL = new List<Label>();

        static TcpClient client;
        static int count = 0;
        static readonly object o = new object();
        static Socket socketCuis;
        static int diff = 130;
        static  List<Button> b;
        static  List<List<String>> platT;
        static  List<List<int>> quT;
        static  List<int> tI;
        static int numT;
        static int bIndex=0;
        List<Button> b2;
        public MainWindow()
        {
            InitializeComponent();
            bf = new BinaryFormatter();
            b = new List<Button>();
            platT = new List<List<String>>();
            quT = new List<List<int>>();
            tI = new List<int>();
            IPHostEntry host = Dns.GetHostEntry("localhost");
            IPAddress ip = new IPAddress(host.AddressList.First().GetAddressBytes());
            new Thread(() =>
            {
                MainWindow.f(this);
            }).Start();
        }
        public MainWindow(List<int> tII, List<List<String>> plattt , List<List<int>> quuu)
        {
            InitializeComponent();
            
            
            bf = new BinaryFormatter();
            b = new List<Button>();
            platT = plattt;
            quT = quuu;
            tI = tII;
            diff = 130;
            List<Label> platL = new List<Label>();
            List<Label> quL = new List<Label>();

            IPHostEntry host = Dns.GetHostEntry("localhost");
            IPAddress ip = new IPAddress(host.AddressList.First().GetAddressBytes());
            
            for( int ii=0; ii<platT.Count; ii++)
            {
                List<String> plat = platT[ii];
                List<int> qu = quT[ii];

                Label lb = new Label();
                lb.Content = "Commande de Table No " + tI[ii];
                lb.FontFamily = new FontFamily("Segoe UI Black");
                lb.FontSize = 30;
                lb.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFF300"));
                lb.Width = 600;
                lb.Height = 60;
                lb.Margin = new Thickness(50, diff, 0, 0);
                p.Children.Add(lb);

                diff += 60;
                for (int i = 0; i < plat.Count; i++)
                {
                    platL.Add(new Label());
                    quL.Add(new Label());

                    platL[i].Content = plat[i];
                    quL[i].Content = qu[i];

                    platL[i].FontFamily = new FontFamily("MV Boli");
                    quL[i].FontFamily = new FontFamily("MV Boli");

                    platL[i].FontSize = 20;
                    quL[i].FontSize = 20;

                    platL[i].Foreground = new SolidColorBrush(Colors.White);
                    quL[i].Foreground = new SolidColorBrush(Colors.White);


                    platL[i].Width = 200;
                    quL[i].Width = 600;


                    platL[i].Height = 40;
                    quL[i].Height = 40;

                    platL[i].Margin = new Thickness(150, diff, 0, 0);
                    quL[i].Margin = new Thickness(100, diff, 0, 0);

                    p.Children.Add(platL[i]);
                    p.Children.Add(quL[i]);

                    diff += 40;

                }
                diff += 20;
                b.Add(new Button());
                b[ii].Content = "Préparer la Comande";
                b[ii].Margin = new Thickness(250, diff, 0, 0);
                b[ii].Width = 200;
                b[ii].Height = 50;
                b[ii].FontSize = 20;
                b[ii].FontFamily = new FontFamily("Segoe UI Black");
                
                p.Children.Add(b[ii]);
                b[ii].Click += new System.Windows.RoutedEventHandler(this.comm_clicked);
            }
            new Thread(() =>
            {
                MainWindow.f(this);
            }).Start();
        }
        public static void f(MainWindow w)
        {
            Thread t = new Thread(() =>
            {
                TcpClient client = new TcpClient("localhost", 2001);
                ns = client.GetStream();
                bIndex = 0;
                var dispatcher = Application.Current.Dispatcher;
                while (true)
                {
                    String tmp = (String)bf.Deserialize(ns);
                    Console.WriteLine(tmp);
                    if (tmp.Equals("commander"))
                    {
                        int numT = (int)bf.Deserialize(ns);
                        List<String> plat = (List<String>)bf.Deserialize(ns);
                        List<int> qu = (List<int>)bf.Deserialize(ns);

                        platT.Add(plat);
                        quT.Add(qu);
                        tI.Add(numT);
                        dispatcher.Invoke((Action)(() =>
                        {
                            Label lb = new Label();
                            lb.Content = "Commande de Table No " + numT;
                            lb.FontFamily = new FontFamily("Segoe UI Black");
                            lb.FontSize = 30;
                            lb.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFF300"));
                            lb.Width = 600;
                            lb.Height = 60;
                            lb.Margin = new Thickness(50, diff, 0, 0);
                            w.p.Children.Add(lb);

                            diff += 60;
                            for (int i = 0; i < plat.Count; i++)
                            {
                                platL.Add(new Label());
                                quL.Add(new Label());

                                platL[i].Content = plat[i];
                                quL[i].Content = qu[i];

                                platL[i].FontFamily = new FontFamily("MV Boli");
                                quL[i].FontFamily = new FontFamily("MV Boli");

                                platL[i].FontSize = 20;
                                quL[i].FontSize = 20;

                                platL[i].Foreground = new SolidColorBrush(Colors.White);
                                quL[i].Foreground = new SolidColorBrush(Colors.White);


                                platL[i].Width = 200;
                                quL[i].Width = 600;


                                platL[i].Height = 40;
                                quL[i].Height = 40;

                                platL[i].Margin = new Thickness(150, diff, 0, 0);
                                quL[i].Margin = new Thickness(100, diff, 0, 0);

                                w.p.Children.Add(platL[i]);
                                w.p.Children.Add(quL[i]);

                                diff += 40;

                            }
                            diff += 20;
                            b.Add(new Button());
                            b[bIndex].Content = "Préparer la Comande";
                            b[bIndex].Margin = new Thickness(250, diff, 0, 0);
                            b[bIndex].Width = 200;
                            b[bIndex].Height = 50;
                            b[bIndex].FontSize = 20;
                            b[bIndex].FontFamily = new FontFamily("Segoe UI Black");
                            w.p.Children.Add(b[bIndex]);
                            b[bIndex].Click += new System.Windows.RoutedEventHandler(w.comm_clicked);
                            bIndex++;
                        }));
                    }
                }

            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }
        private void comm_clicked(object sender, RoutedEventArgs e)
        {
                    Button tmp = (Button)sender;
                    if (tmp.Content.Equals("Préparer la Comande") ){
                        tmp.Content = "Servir la commande";
                        bf.Serialize(ns, "prepa");
                    }
                    else
                    {
                        bf.Serialize(ns, "serv");
                        platT.RemoveAt(0);
                        quT.RemoveAt(0);
                        tI.RemoveAt(0);
                        b.RemoveAt(0);
                        new MainWindow(tI, platT, quT).Show();
                        this.Close();

                    }
                }
            }
        }
        
        /*
        public static void ServerService()
        {
            Socket socket = clients.AcceptSocket();

            Console.WriteLine("Connected " + socket.RemoteEndPoint);

            Stream ns = new NetworkStream(socket);


            var bf = new BinaryFormatter();

            bool client = false;

            Thread t = new Thread(() =>
            {
                while (!client)
                {
                    // client -> cuisinier
                    String tmp = (String)bf.Deserialize(ns);
                    if (tmp.Equals("commander"))
                    {
                        int numT = (int)bf.Deserialize(ns);
                        List<String> plat = (List<String>)bf.Deserialize(ns);
                        List<int> qu = (List<int>)bf.Deserialize(ns);
                        Label lb = new Label();
                        lb.Content = "Commande de Table No " + numT;
                        lb.FontFamily = new FontFamily("Segoe UI Black");
                        lb.FontSize = 30;
                        lb.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFF300"));
                        lb.Width = 600;
                        lb.Height = 40;
                        lb.Margin = new Thickness(50, diff, 0, 0);
                        p.Children.Add(lb);

                        diff += 10;
                        for (int i = 0; i < plat.Count; i++)
                        {
                            platL.Add(new Label());
                            quL.Add(new Label());

                            platL[i].Content = plat[i];
                            quL[i].Content = qu[i];

                            platL[i].FontFamily = new FontFamily("MV Boli");
                            quL[i].FontFamily = new FontFamily("MV Boli");

                            platL[i].FontSize = 20;
                            quL[i].FontSize = 20;

                            platL[i].Foreground = new SolidColorBrush(Colors.White);
                            quL[i].Foreground = new SolidColorBrush(Colors.White);


                            platL[i].Width = 200;
                            quL[i].Width = 600;


                            platL[i].Height = 40;
                            quL[i].Height = 40;

                            platL[i].Margin = new Thickness(100, diff, 0, 0);
                            quL[i].Margin = new Thickness(50, diff, 0, 0);

                            p.Children.Add(platL[i]);
                            p.Children.Add(quL[i]);

                            diff += 40;




                        }

                    }
                }
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }
    */
  